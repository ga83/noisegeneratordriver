package com.tuxware.kirra;

public class NoiseGeneratorDriver {
    public static final long SEED_FIRST = 69;
    private static final int GAMEPIECE_NOISE_TEXTURE_WIDTH = 256;
    private static final int GAMEPIECE_NOISE_TEXTURE_HEIGHT = 256;
    private static final float GAMEPIECE_NOISE_SIZE = 8;
    private static final float GAMEPIECE_NOISE_CONTRAST = 0.55f;
    private static final int NUMBER_OF_GAMEPIECES = 117;


    public static void main(String[] parameters) {

        NoiseGenerator ng = new NoiseGenerator();

        for(int i = 0; i < NUMBER_OF_GAMEPIECES; i++) {
            ng.generate(
                    SEED_FIRST + i,
                    GAMEPIECE_NOISE_TEXTURE_WIDTH,
                    GAMEPIECE_NOISE_TEXTURE_HEIGHT,
                    GAMEPIECE_NOISE_SIZE,
                    GAMEPIECE_NOISE_CONTRAST,
                    "output/gamepiece" + (i < 10 ? "0" + i : i) + ".png"
            );
        }
    }
}
