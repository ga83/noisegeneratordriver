package com.tuxware.kirra;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * In general, generates a monochrome noise texture. It can be tinted within shaders, or elsewhere.
 */
public class NoiseGenerator {


    /**
     * Note: this is only the standalone version which runs on the desktop just to output a png.
     * Generates a perlin noise pixmap in this object.
     * Contrast should always be less than 1 - luminanceMinimum, otherwise values will exceed 1, which is not allowed.
     */
    public void generate(long seed, int width, int height, float noiseSize, float contrast, String fileName) {
        long timeBegin = System.currentTimeMillis();

        Random rnd = new Random(seed);
        rnd.nextInt();  // improve randomisation.

        float xOffset = 0 + rnd.nextFloat() * (1.0f - noiseSize);
        float yOffset = 0 + rnd.nextFloat() * (1.0f - noiseSize);
        //Pixmap pixmap = new Pixmap(width, height, Pixmap.Format.RGBA8888);
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        for(int x = 0; x < width; x++) {
            float xCoordinate = xOffset + (x / (float)width) * noiseSize;

            for(int y = 0; y < height; y++) {
                float yCoordinate = yOffset + (y / (float)height) * noiseSize;

                float noiseValue = (float) ImprovedNoise.noise(xCoordinate, yCoordinate, 0);
                noiseValue += 1;    // bring all values to between 0 and 2.
                noiseValue *= 0.5f; // bring all values to between 0 and 1.
                noiseValue *= contrast; // reduce contrast.
                noiseValue += (1.0f - contrast); // brighten.

                //pixmap.setColor(noiseValue, noiseValue, noiseValue, 1.0f);
                //pixmap.drawPixel(x, y);
                Color color = new Color(
                        (int)(noiseValue * 255),
                        (int)(noiseValue * 255),
                        (int)(noiseValue * 255),
                        255
                );
                bufferedImage.setRGB(x, y, color.getRGB());
            }
        }

        long timeEnd = System.currentTimeMillis();

        System.out.println("====== time to generate noise: " + ((timeEnd - timeBegin) / 1000f));

        //return pixmap;
        try {
            ImageIO.write(bufferedImage,"png", new File(fileName));
        } catch (IOException e) {
            System.out.println("error: " + e.getMessage());
        }
    }

    /*
    public void saveFile() {

        boolean isExtAvailable = Gdx.files.isExternalStorageAvailable();
        boolean isLocAvailable = Gdx.files.isLocalStorageAvailable();

        String extRoot = Gdx.files.getExternalStoragePath();
        String locRoot = Gdx.files.getLocalStoragePath();

        System.out.println("====== local storage: " + isLocAvailable + ", " + locRoot);

        System.out.println("====== saving file....");

        FileHandle levelHandleOutput = Gdx.files.local("./noise_" + "seed" + ".png");
        PixmapIO.writePNG(levelHandleOutput, this.noisePixmap, Deflater.DEFAULT_COMPRESSION, false);

        System.out.println("====== file saved.");
    }
     */
}
